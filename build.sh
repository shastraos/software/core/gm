#!/bin/bash


# Check if there is a build/ directory
if [ -d "build" ]; then
    echo "Deleting existing build/ directory... will rebuild..."
    rm -rf build
fi

# Compile resources
echo "Compiling resources..."
glib-compile-resources shastraos-gm.gresource.xml

# Run `meson build`
echo "Running meson..."
meson build

# Run `ninja -C build install`
if [ $? -eq 0 ]; then
    echo "Running ninja and installing..."
    ninja -C build install
else
    echo "Meson configuration failed. Aborting build."
fi
