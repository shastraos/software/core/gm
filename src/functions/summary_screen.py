# finish.py
#
# Copyright 2023 Sam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk, Adw


@Gtk.Template(resource_path='/org/shastraos/gm/pages/summary_screen.ui')
class SummaryScreen(Adw.Bin):
    __gtype_name__ = 'SummaryScreen'
    phrase_gen = Gtk.Template.Child()
    copy_button = Gtk.Template.Child()
    next_button = Gtk.Template.Child()
    back_button = Gtk.Template.Child()
    
    def __init__(self, window, main_carousel, application, **kwargs):
        super().__init__(**kwargs)
        self.window = window
        self.carousel = main_carousel
        
        self.copy_button.connect("clicked", self.copy_recovery_phrase)
        self.next_button.connect("clicked", self.next_button_clicked)
        self.back_button.connect("clicked", self.back_button_clicked)

        self.text_buffer = self.phrase_gen.get_buffer()
        
        # temporary -----------------
        self.text_buffer.set_text("arrive father unlucky pattern localize lesson\nprefer bad model box banana mansion")
        # ---------------------------
        
    def copy_recovery_phrase(self, *args):
        pass
        
    
     
    def next_button_clicked(self, *args):
        # save the account details
        # self.carousel.scroll_to(self.carousel.get_nth_page(5), True)
        
        #temporary -----------------
        self.window.destroy()
        # ---------------------------
        
    def back_button_clicked(self, *args):
        self.carousel.scroll_to(self.carousel.get_nth_page(3), True)